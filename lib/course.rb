# Students and Courses
#
# Write a set of classes to model `Student`s and `Course`s.
#
# When you are finished with the extras, comment in line 7 in the
# `spec/course_spec.rb` file and run all of the specs at once with `bundle
# exec rspec`. Make sure everything passes!

# ## Course
# * `Course#initialize` should take the course name, department, and
#   number of credits.
# * `Course#students` should return a list of the enrolled students.
# * `Course#add_student` should add a student to the class.
#   * Probably can rely upon `Student#enroll`.
#
# ## And some extras:
# * Each course should also take a set of days of the week (`:mon`,
#   `:tue`, ...), plus a time block (assume each day is broken into 8
#   consecutive time blocks). So a course could meet
#   `[:mon, :wed, :fri]` during block #1.
#     * Update your `#initialize` method to also take a time block and
#       days of the week.
# * Write a method `Course#conflicts_with?` which takes a second
#   `Course` and returns true if they conflict.


class Course

  attr_accessor :name, :department, :credits, :students, :days, :time_block

  def initialize(name, department, credits, days, time_block)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

  def add_student(student)
    student.enroll(self)
  end

  def conflicts_with?(course2)
    if @time_block == course2.time_block
      @days.each do |day1|
        course2.days.each do |day2|
          return true if day1 == day2
        end
      end
    end
    false
  end

end
